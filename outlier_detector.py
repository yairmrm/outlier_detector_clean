from __future__ import division

#from pyspark import SparkContext
import copy
import time
import os
import sys
import numpy as np
import yaml

from coreset_streamer import CoresetStreamer
from set_of_points import SetOfPoints
from parameters_config import ParameterConfig
from data_cleaner import DataCleaner
from input_reader import InputReader




class OutliersDetector:
    parameters_config = None
    current_iteration = None

    @staticmethod
    def EM_estimator_k_means_robust(P, parameters_config=None, is_ground_truth = True, EM_iterations = np.nan):
        print("find centers and outliers..")
        print("points number: " + str(P.get_size()))
        if parameters_config == None:
            OutliersDetector.init_parameter_config()
            parameters_config = OutliersDetector.parameters_config
        centers_number = parameters_config.centers_number
        outliers_number = parameters_config.outliers_number
        if is_ground_truth:
            EM_iterations = P.get_size() ** 2
        size = P.get_size()
        min_cost = np.inf
        min_k_centers = SetOfPoints()
        for i in range(EM_iterations):
            if int(i % (EM_iterations/10)) == 0:
                print(str(int(float(i)/float(EM_iterations)*100)) + " % done")
            k_centers = P.get_sample_of_points(centers_number)
            closest = P.get_closest_points_to_set_of_points(k_centers, size - outliers_number, type="by number")
            if closest.get_size() == 0:
                continue
            current_cost = closest.get_sum_of_distances_to_set_of_points(k_centers)
            if current_cost < min_cost:
                min_cost = current_cost
                min_k_centers = k_centers
        if min_k_centers.get_size() == 0:
            return [SetOfPoints(), SetOfPoints()]
        outliers = P.get_farthest_points_to_set_of_points(min_k_centers, outliers_number, type="by number")
        print("finished.")
        return [min_k_centers, outliers]

    @staticmethod
    def RANSAC(P, sample_size):
        parameters_config = OutliersDetector.parameters_config
        outliers_number = parameters_config.outliers_number
        min_cost = np.infty
        P_size = P.get_size()
        RANSAC_steps = int(np.sqrt(sample_size))
        counter = 0
        while True:
            R = P.get_sample_of_points(sample_size)
            R.weights = R.weights.reshape(-1)
            [centers, outliers] = OutliersDetector.EM_estimator_k_means_robust(R, is_ground_truth=False, EM_iterations = 1)
            closest = P.get_closest_points_to_set_of_points(centers,P_size-outliers_number,"by number")
            current_cost = closest.get_sum_of_distances_to_set_of_points(centers)
            if min_cost > current_cost:
                min_cost = current_cost
                R_min = R
                counter = 0
            counter += 1
            if counter == RANSAC_steps:
                break

        return R_min

    @staticmethod
    def run_corset(P, sample_size, parameters_config = None, RANSAC=False):
        if parameters_config == None:
            parameters_config = OutliersDetector.parameters_config
        points_number = parameters_config.points_number
        k = parameters_config.centers_number + parameters_config.outliers_number
        min_coreset_cost = np.infty
        coreset_total_time = 0
        coreset_starting_time = time.time()
        C = CoresetStreamer(sample_size=sample_size, points_number=points_number, k=k,
                            parameters_config=parameters_config).stream(P)
        coreset_ending_time = time.time()
        coreset_total_time += coreset_ending_time - coreset_starting_time
        [coreset_means, coreset_outliers] = OutliersDetector.EM_estimator_k_means_robust(C, parameters_config)
        if coreset_means.get_size() == 0:
            return [np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan]
        coreset_means.set_all_weights_to_specific_value(1.0)
        coreset_outliers.set_all_weights_to_specific_value(1.0)
        # print("coreset_means.points: \n", coreset_means.points)
        # print("coreset_outliers.points: \n", coreset_outliers.points)
        corset_cost = P.get_cost_to_center_without_outliers(coreset_means, coreset_outliers)

        random_sample_cost = RANSAC_total_time = random_sample_outliers = None
        if RANSAC:
            RANSAC_total_time = 0
            RANSAC_starting_time = time.time()
            R = OutliersDetector.RANSAC(P, sample_size)
            RANSAC_ending_time = time.time()
            RANSAC_total_time += RANSAC_ending_time - RANSAC_starting_time
            R.weights = R.weights.reshape(-1)
            [random_sample_means, random_sample_outliers] = OutliersDetector.EM_estimator_k_means_robust(R)
            print("random_sample_means.points: \n", random_sample_means.points)
            print("random_sample_outliers.points: \n", random_sample_outliers.points)
            random_sample_cost = P.get_cost_to_center_without_outliers(random_sample_means, random_sample_outliers)
            return [corset_cost, random_sample_cost, coreset_total_time, RANSAC_total_time, coreset_outliers,
                    random_sample_outliers]
        return [corset_cost, 0, coreset_total_time, 0, coreset_outliers, np.asarray([]), coreset_means, np.asarray([])]


    @staticmethod
    def get_points_from_file(inputfolder):

        # Read all files from input folder as one list object
        mrpink = InputReader()
        rows_list = mrpink.get_row_list()

        # Process/Clean data from files
        mrwolf = DataCleaner(rows_list)
        mrwolf.clean_data()
        complete_MTRX = mrwolf.get_complete_matrix()

        if not complete_MTRX.any():
            raise Exception("Error! complete_MTRX is empty")
        else:
            P = SetOfPoints(complete_MTRX)
            return P

    @staticmethod
    def init_parameter_config():

        parameters_config = ParameterConfig()
        # main parameters
        parameters_config.points_number = -1
        parameters_config.header_indexes = [0, 1, 2, 3, 5]
        # experiment  parameters
        parameters_config.sample_size = 30
        parameters_config.inner_iterations = 5
        parameters_config.centers_number = 3
        parameters_config.outliers_number = 10
        # coreset parameters
        parameters_config.median_sample_size = 1
        parameters_config.closest_to_median_rate = 0.99
        parameters_config.number_of_remains_multiply_factor = 1
        parameters_config.max_sensitivity_multiply_factor = 2
        # iterations
        parameters_config.RANSAC_EM_ITERATIONS = 10
        parameters_config.RANSAC_loops_number = 10
        # files
        parameters_config.input_points_file_name = os.getcwd() + "/dataset/HDR_points.csv"
        """
        with io.open('outlier_detector_config.yml', 'w', encoding='utf8') as outfile:
            yaml.dump(parameters_config, outfile, default_flow_style=False, allow_unicode=True)
        
        with open("outlier_detector_config.yml", 'r') as stream:
            parameters_config = yaml.load(stream)
        """
        OutliersDetector.parameters_config = parameters_config

    @staticmethod
    def detect(P):
        # parameters
        OutliersDetector.init_parameter_config()
        parameters_config= OutliersDetector.parameters_config
        # Fix: run data cleaner before
        input_points_file_name = parameters_config.input_points_file_name
        # P = OutliersDetector.get_points_from_file(os.getcwd() + "/dataset/")
        #P = SetOfPoints(complete_MTRX)
        sample_size = parameters_config.sample_size
        [C_cost, _, coreset_total_time, _, C_outliers, _, coreset_means, RANSAC_means] \
            = OutliersDetector.run_corset(P=P, sample_size=sample_size)
        return C_cost, coreset_total_time, C_outliers, coreset_means

    @staticmethod
    def coreset_in_cluster(P, parameters_config):
        C = CoresetStreamer(sample_size=300, points_number=P.get_size(), k=2,
                            parameters_config=parameters_config).stream(P)
        return C

    @staticmethod
    def k_means_in_cluster(C, parameters_config):
        C_cost, _, coreset_total_time, _, C_outliers, _, coreset_means, RANSAC_means \
            = OutliersDetector.run_corset(P=C, sample_size=parameters_config.sample_size, parameters_config=parameters_config)
        return C_cost, coreset_total_time, C_outliers, coreset_means, RANSAC_means


    @staticmethod
    def detect_via_spark(rdd, sc):
        """
        funcs = []
        for i in [1, 2]:
            f = f_factory(i)
            funcs.append(f)
        arr = [[1, 2], [1, 4], [1, 6], [2, 8], [2, 10], [2, 12], [2, 14], [2, 16]]
        clean_rdd = sc.parallelize(arr)
        rdds = []
        for f in funcs:
            rdds.append(clean_rdd.filter(f))
        for r in rdds:
            x = np.asarray(r.collect())
            u = 2
        """
        OutliersDetector.init_parameter_config()
        parameters_config = OutliersDetector.parameters_config
        #workers_coresets = rdd.map(lambda x: OutliersDetector.coreset_in_cluster(x, parameters_config)).partitionBy(int(rdd.count()/2)).collect()
        print("******* after4")
        workers_coresets = rdd.map(lambda x: OutliersDetector.coreset_in_cluster(x, parameters_config)).collect()
        C = SetOfPoints()
        for coreset in workers_coresets:
            C.add_set_of_points(coreset)
        workers_coresets = sc.parallelize([C])
        results = workers_coresets.map(lambda x: OutliersDetector.k_means_in_cluster(x, parameters_config)).collect()
        C_cost = results[0][0]
        coreset_total_time = results[0][1]
        C_outliers = results[0][2]
        coreset_means = results[0][3]
        return C_cost, coreset_total_time, C_outliers, coreset_means

def f_factory(i):
    def f(x):
        return x[0] == i  # i is now a *local* variable of f_factory and can't ever change
    return f