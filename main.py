import numpy as np
import os
import es_end2end
from es_end2end import ODProccessManager

#data_path_full = '/home/outlier_detector_clean/datasets/kddcup.data_10_percent_corrected_sample'
#data_path = 'datasets/kddcup.data_10_percent_corrected_sample'
from set_of_points import SetOfPoints

selected_features_indexes = [36] # [13, 16, 36]


class ODProccessManagerTest(ODProccessManager):

    def __init__(self, is_reset_indexes = False):
        ODProccessManager.__init__(self, is_reset_indexes=is_reset_indexes)

    def cleane_row(self, raw_vector):
        try:
            cells = raw_vector[1].split(',')
        except Exception as e:
            print("The error is: ", e)
        clean_vector = []
        for i in selected_features_indexes:
            clean_vector.append(float(cells[i]))
        return clean_vector

if __name__ == "__main__":
    od = ODProccessManagerTest(is_reset_indexes=False)
    q = '{"query":{"match_all":{}}}'
    data_clean_rdd = od.read_data_from_index(q, 'clean')
    print("data_clean_rdd.collect(): ", data_clean_rdd.collect())
    data_clean = np.asarray(data_clean_rdd.collect())
    P = []
    for d in data_clean[:,1]:
        P.append(d["a"])
    P = np.asarray(P).reshape(-1,1)
    print("******* after1")
    P = es_end2end.sc.parallelize([SetOfPoints(P)])
    print("******* after2")
    error, coreset_total_time, C_outliers, coreset_means = od.detect_anomalies_spark(P)
    print("******* after3")
    anomalies_rdd = es_end2end.sc.parallelize([C_outliers.points])
    anomalies_rdd_2 = es_end2end.sc.parallelize(anomalies_rdd.map(od.anomaly_to_index_presentation).collect())
    print("anomalies_rdd.collect(): ", anomalies_rdd_2.collect())
    od.write_data_at_index(anomalies_rdd_2, 'anomalies')

    print("error: ", error)
    print("coreset_total_time: ", coreset_total_time)
    print("C_outliers: ", C_outliers.points)
    print("C_outliers.indexes: ", C_outliers.indexes)
    print("coreset_means: ", coreset_means.points)
    print("coreset_indexes: ", coreset_means.indexes)




"""
points = np.arange(0,1000,1).reshape(-1,1)
    for i in range(100):
        e = {"a" : 1*i,
              "b" : 2*i,
              "c" : 3*i,
              "d" : 4*i,
              "e" : 5*i}
        res = es_end2end.es.index(index='clean', doc_type='sample', id=i, body=e)
"""