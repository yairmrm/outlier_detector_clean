from pyspark import SparkConf, SparkContext  # , SparkConf
import numpy as np
import sys


def create_sc():
    sc_conf = SparkConf()
    sc_conf.setAppName("spark-test-app")
    sc_conf.setMaster('spark://172.31.45.81:7077')
    sc_conf.set('spark.executor.memory', '2g')
    sc_conf.set('spark.executor.cores', '2')
    sc_conf.set('spark.cores.max', '40')
    sc_conf.set('spark.logConf', True)
    print(sc_conf.getAll())

    sc = None
    try:
        sc.stop()
        sc = SparkContext(conf=sc_conf)
    except:
        sc = SparkContext(conf=sc_conf)

    return sc

def my_func(P):
    C = np.linalg.pinv(P)
    return C

if __name__ == "__main__":
    #sc = create_sc()
    print("******  started main ******")
    sc = SparkContext(appName="sparktest")
    total = []
    machines_number = 2 #int(sys.argv[1])
    P = np.random.rand(400,100)
    p_size = len(P)
    chunk_size = int(p_size/machines_number)
    for i in range(0, p_size, chunk_size):
        current_points = P[i:i+chunk_size]
        total.append(current_points)
    data = sc.parallelize(total)
    C = data.map(lambda x: my_func(x)).collect()
    for i in range(len(C)):
        print(np.mean(C[i]))
    print("******  finished main ******")
    print("the number of machines we've got:", sys.argv[1])
