from __future__ import division

import copy
import math
import os
import random
import time
import numpy as np
import sys
from pyspark import SparkContext
import csv



class CoresetSparkExperiment:
    """
    A class that includes all the main experiments of the weighted centers corset API
    """

    ######################################################################

    @staticmethod
    def EM_estimator_k_means_robust(P, is_ground_truth=True, EM_iterations=np.nan):
        """
        The state of the art algorithm to finding a robust median without outliers.
        Args:
            P (SetOfPoints) : set of weighted points
            k (int) : number of weighted centers

        Returns:
            np.ndarray: the robust median of P
        """
        parameter_config = CoresetSparkExperiment.parameter_config
        centers_number = parameter_config.centers_number
        outliers_number = parameter_config.outliers_number
        P_size = P.get_size()
        if is_ground_truth:
            EM_iterations = P_size ** 2
        k_centers = P.get_sample_of_points(centers_number)
        closest = P.get_closest_points_to_set_of_points(k_centers, P_size - outliers_number, type="by number")
        min_cost = k_centers.get_sum_of_distances_to_set_of_points(closest)
        min_k_centers = k_centers
        for i in range(EM_iterations):
            k_centers = P.get_sample_of_points(centers_number)
            closest = P.get_closest_points_to_set_of_points(k_centers, P_size - outliers_number, type="by number")
            current_cost = closest.get_sum_of_distances_to_set_of_points(k_centers)
            if current_cost < min_cost:
                min_cost = current_cost
                min_k_centers = k_centers
        outliers = P.get_farthest_points_to_set_of_points(min_k_centers, outliers_number, type="by number")
        return [min_k_centers, outliers]

    ######################################################################

    @staticmethod
    def RANSAC(P, sample_size, required_time):
        """
        TODO: complete
        :param P:
        :param k:
        :param sample_size:
        :return:
        """
        parameter_config = CoresetSparkExperiment.parameter_config
        outliers_number = parameter_config.outliers_number
        min_cost = np.infty
        P_size = P.get_size()
        ransac_total_time = 0
        while True:
            RANSAC_starting_time = time.time()
            R = P.get_sample_of_points(sample_size)
            R.weights = R.weights.reshape(-1)
            [centers, outliers] = CoresetSparkExperiment.EM_estimator_k_means_robust(R, is_ground_truth=False, EM_iterations=1)
            closest = P.get_closest_points_to_set_of_points(centers, P_size - outliers_number, "by number")
            #current_cost = closest.get_sum_of_distances_to_set_of_points(centers)
            current_cost = P.get_cost_to_center_without_outliers(centers, outliers)
            if min_cost > current_cost:
                min_cost = current_cost
                R_min = R
                counter = 0
            RANSAC_ending_time = time.time()
            ransac_total_time += RANSAC_ending_time - RANSAC_starting_time
            if ransac_total_time >= required_time:
                break
        return R_min, ransac_total_time

    ######################################################################

    @staticmethod
    def get_coreset(P, sample_size, parameter_config):
        print("started")
        starting_time = time.time()
        C = CoresetStreamer(sample_size=sample_size, points_number=parameter_config.points_number, k=parameter_config.centers_number, parameters_config=parameter_config).stream(P)
        ending_time = time.time()
        print("finished")
        return C, starting_time, ending_time

    ######################################################################

    @staticmethod
    def run_corset_and_RANSAC(P, P_parallel, sample_size, coreset_iterations, RANSAC_iterations):
        """
        TODO: complete
        In this experiment, we get a set P consist of weighted points with outliers, sample randomly a set S_1
        of m points from P, and sample a coreset S_2 consist of m points from P. Run the the recursive median on S_1 to
        find a median q_1, and run the recursive median on S_2 to find a median q_2, and compare the robust cost from q_1
        to P (ignoring the k farthest points) versus the robust cost from q_2 to P (ignoring the k farthest points).
        Args:
            k (int) : number of weighted centers
            sample_size (int) : size of sample
        Returns:
            [float, float, float] : the coreset total cost, the RANSAC total cost, and the ground truth total cost
        """

        parameter_config = CoresetSparkExperiment.parameter_config
        points_number = parameter_config.points_number
        k = parameter_config.centers_number + parameter_config.outliers_number
        min_coreset_cost = np.infty
        coreset_total_time = 0
        for i in range(coreset_iterations):
            coreset_starting_time = time.time()
            #C = CoresetStreamer(sample_size=sample_size, points_number=points_number, k=k,parameter_config=parameter_config).stream(P)
            answers = P_parallel.map(lambda x: CoresetSparkExperiment.get_coreset(x, sample_size, parameter_config)).collect()
            C = SetOfPoints()
            for ans in answers:
                C.add_set_of_points(ans[0])
            coreset_ending_time = time.time()
            coreset_total_time += coreset_ending_time - coreset_starting_time
            [coreset_means, coreset_outliers] = CoresetSparkExperiment.EM_estimator_k_means_robust(C)
            coreset_means.set_all_weights_to_specific_value(1.0)
            coreset_outliers.set_all_weights_to_specific_value(1.0)
            print("coreset_means.points: \n", coreset_means.points)
            print("coreset_outliers.points: \n", coreset_outliers.points)
            current_corset_cost = P.get_cost_to_center_without_outliers(coreset_means, coreset_outliers)
            if min_coreset_cost > current_corset_cost:
                min_coreset_cost = current_corset_cost
        coreset_cost = min_coreset_cost

        RANSAC_total_time = 0
        min_random_sample_cost = np.infty
        for i in range(RANSAC_iterations):
            R, RANSAC_total_time = CoresetSparkExperiment.RANSAC(P, C.get_size(), coreset_total_time)
            R.weights = R.weights.reshape(-1)
            [random_sample_means, random_sample_outliers] = CoresetSparkExperiment.EM_estimator_k_means_robust(R)
            print("random_sample_means.points: \n", random_sample_means.points)
            print("random_sample_outliers.points: \n", random_sample_outliers.points)
            current_random_sample_cost = P.get_cost_to_center_without_outliers(random_sample_means,
                                                                                        random_sample_outliers)
            if min_random_sample_cost > current_random_sample_cost:
                min_random_sample_cost = current_random_sample_cost
        random_sample_cost = min_random_sample_cost

        return [coreset_cost, random_sample_cost, coreset_total_time, RANSAC_total_time, coreset_outliers.points, random_sample_outliers.points]

    ######################################################################

    @staticmethod
    def run_main_experiment(P, P_parallel):
        """
        In this experiment, we get an uncompleted matrix D with one randomly missing entry in each observation in D.
        We are running state of the art matrix completion algorithms on our coreset sample, and on a sample we get from
        RANSAC as well, and compare the solution accuracy versus the size of sample.
        :return:
        """
        parameter_config = CoresetSparkExperiment.parameter_config

        # parameters
        points_number = parameter_config.points_number
        coreset_iterations = parameter_config.coreset_iterations
        RANSAC_iterations = parameter_config.RANSAC_iterations
        inner_iterations = parameter_config.inner_iterations

        # containers for statistics
        C_error_totals_final = []  # coreset total average error at each iteration
        C_error_totals_var = []  # coreset total variance for each sample
        random_sample_error_totals_final = []  # RANSAC total average error at each iteration
        random_sample_error_totals_var = []  # RANSAC total variance for each sample
        coreset_totals_time = []  # RANSAC total variance for each sample
        RANSAC_totals_time = []  # RANSAC total variance for each sample
        sample_sizes = []
        points_numbers = []
        ground_truth_cost = 1
        for u in range(len(parameter_config.sample_sizes)):
            CoresetSparkExperiment.current_iteration = u
            sample_size = parameter_config.sample_sizes[u]
            print("iteration number ", u)
            C_error_total = []
            random_sample_error_total = []
            coreset_total_time_inner = []
            RANSAC_total_time_inner = []
            for t in range(inner_iterations):
                print("inner iteration number ", t)
                [C_cost, random_sample_cost, coreset_total_time, RANSAC_total_time, C_outliers, RANSAC_outliers] = \
                    CoresetSparkExperiment.run_corset_and_RANSAC(P=P,
                                                                 P_parallel=P_parallel,
                                                                 sample_size=sample_size,
                                                                 coreset_iterations=coreset_iterations,
                                                                 RANSAC_iterations=RANSAC_iterations)
                C_error = C_cost / ground_truth_cost
                random_sample_error = random_sample_cost / ground_truth_cost
                C_error_total.append(C_error)
                random_sample_error_total.append(random_sample_error)
                coreset_total_time_inner.append(coreset_total_time)
                RANSAC_total_time_inner.append(RANSAC_total_time)
            # avgs
            C_error_total_avg = np.mean(C_error_total)
            C_error_total_var = np.var(C_error_total) / C_error_total_avg
            random_sample_error_total_avg = np.mean(random_sample_error_total)
            random_sample_error_total_var = np.var(random_sample_error_total) / random_sample_error_total_avg
            coreset_total_time_inner_avg = np.mean(coreset_total_time_inner)
            RANSAC_total_time_inner_avg = np.mean(RANSAC_total_time_inner)
            C_error_totals_final.append(C_error_total_avg)
            C_error_totals_var.append(C_error_total_var)
            random_sample_error_totals_final.append(random_sample_error_total_avg)
            random_sample_error_totals_var.append(random_sample_error_total_var)
            coreset_totals_time.append(coreset_total_time_inner_avg)
            RANSAC_totals_time.append(RANSAC_total_time_inner_avg)
            sample_sizes.append(sample_size)
            points_numbers.append(points_number)
            coreset_to_RANSAC_ratio = list(np.asarray(C_error_totals_final) / np.asarray(random_sample_error_totals_final))
            # information printing
            print("points_numbers = ", points_numbers)
            print("sample_sizes = ", sample_sizes)
            print("coreset_to_RANSAC_ratio = ", coreset_to_RANSAC_ratio)
            print("C_error_totals_final = ", C_error_totals_final)
            print("random_sample_error_totals_final = ", random_sample_error_totals_final)
            print("C_error_totals_var =  ", C_error_totals_var)
            print("random_sample_error_totals_var = ", random_sample_error_totals_var)
            print("coreset_totals_time = ", coreset_totals_time)
            print("RANSAC_totals_time = ", RANSAC_totals_time)
            print("coreset_to_RANSAC_time_rate = ", np.asarray(coreset_totals_time) / np.asarray(RANSAC_totals_time))

    ######################################################################

    @staticmethod
    def get_synthetic_data(points_number):
        """
        Args:
            points_number (int) : number of lines to generate

        Returns:
            SetOfLines: syntheticly generated set of lines_number lines
        """
        dim = 3

        outliers_lines_number = 2
        main_points_number = points_number - outliers_lines_number

        j = -0.5
        points = []
        for i in range(main_points_number):
            if i == 0:
                points = np.array([[j, 0, 0]])
            else:
                points = np.concatenate((points, np.array([[j, 0, 0]])), axis=0)
            j = j + (1 / main_points_number)
        outliers_indexes = np.asarray(range(outliers_lines_number)) + 1
        for i in outliers_indexes:
            points = np.concatenate((points, np.array([[100000000 * (i+1), 0, 0]])), axis=0)
        return SetOfPoints(points)

    ######################################################################

    @staticmethod
    def get_data_from_file(points_number):
        """
        :param points_number:
        :return: D the complete matrix
        """
        D = []
        parameter_config = CoresetSparkExperiment.parameter_config
        header_indexes = parameter_config.header_indexes
        with open(parameter_config.input_points_file_name, 'rt') as csvfile:
            spamreader = [row for row in csv.reader(csvfile.read().splitlines())]
            i = 0
            for row in spamreader:
                if i==0:
                    i += 1
                    continue
                if row == []:
                    continue
                clean_row = [element.strip() for element in row]
                row_arr = np.asarray(clean_row)
                row_arr_first_dim_cols = row_arr[header_indexes]
                D.append(copy.deepcopy(row_arr_first_dim_cols))
                i += 1
                if i == int(points_number):
                    break
            D = np.asarray([[float(i) for i in point] for point in D])
        return SetOfPoints(D)

    ######################################################################


    @staticmethod
    def init_parameter_config():
        """
        TODO: complete
        :param experiment_type:
        :return:
        """

        parameter_config = ParameterConfig()

        # main parameters
        parameter_config.header_indexes = [1, 2, 3]
        parameter_config.dim = len(parameter_config.header_indexes)
        parameter_config.points_number = 400000

        # coreset parameters
        parameter_config.a_b_approx_minimum_number_of_lines = 1
        parameter_config.sample_size_for_a_b_approx = 20
        parameter_config.farthest_to_centers_rate_in_a_b_approx = 0.1
        parameter_config.inner_a_b_approx_iterations = 5

        # experiment  parameters
        if is_amr:
            parameter_config.sample_sizes = [] #[250, 500, 750, 1000, 1250, 1500, 1750, 2000]
        else:
            parameter_config.sample_sizes = [100]
        parameter_config.inner_iterations = 5
        parameter_config.centers_number = 5
        parameter_config.outliers_number = 4

        # missing entries parameters
        parameter_config.missing_entries_alg = 'KNN'  # 'FancyImpute' #
        parameter_config.cost_type = 'matrices_diff'  # 'k_means'  #
        parameter_config.KNN_k = 5

        # EM k means for lines estimator parameters
        parameter_config.multiplications_of_k = parameter_config.centers_number
        parameter_config.ground_true_iterations_number_ractor = 10

        # weighted centers coreset parameters
        parameter_config.median_sample_size = 1
        parameter_config.closest_to_median_rate = 1 - parameter_config.farthest_to_centers_rate_in_a_b_approx
        parameter_config.number_of_remains_multiply_factor = 1
        parameter_config.number_of_remains = 3
        parameter_config.max_sensitivity_multiply_factor = 2

        # iterations
        parameter_config.RANSAC_iterations = 1
        parameter_config.coreset_iterations = 1
        parameter_config.RANSAC_inner_iterations = 30
        parameter_config.coreset_to_ransac_time_rate = 1

        # files
        #parameter_config.input_points_file_name = 'Phones_accelerometer_normalized_0.5_M.csv'
        if is_amr:
            #parameter_config.input_points_file_name = 's3://yair-marom-tests/outlier_detector/Phones_gyroscope.csv'
            #parameter_config.input_points_file_name = "s3://yair-marom-tests/outlier_detector/Phones_accelerometer_normalized_0.5_M.csv"
            parameter_config.input_points_file_name = "Phones_accelerometer_normalized_0.5_M.csv"
        else:
            #parameter_config.input_points_file_name = '/home/yair/python_projects/datasets/Phones_gyroscope.csv'
            parameter_config.input_points_file_name = '/home/yair/python_projects/datasets/Phones_accelerometer_normalized_0.5_M.csv'

        CoresetSparkExperiment.parameter_config = parameter_config
        return parameter_config

    ######################################################################
















if __name__ == "__main__":
    is_amr = True
    print("******  started main ******")
    sc = SparkContext()
    if not is_amr:
        machines_number = 2
        import matplotlib as plt
    else:
        print("sys.argv[0]: ", sys.argv[0], "sys.argv[1]: ", sys.argv[1], "sys.argv[2]: ", sys.argv[2], "sys.argv[3]: ", sys.argv[3])
        machines_number = int(sys.argv[1])
        print("sc started to load files..")
        sc.addFile("s3://yair-marom-tests/outlier_detector/coreset_for_weighted_centers.py")
        sc.addFile("s3://yair-marom-tests/outlier_detector/coreset_node.py")
        sc.addFile("s3://yair-marom-tests/outlier_detector/coreset_streamer.py")
        sc.addFile("s3://yair-marom-tests/outlier_detector/parameters_config.py")
        sc.addFile("s3://yair-marom-tests/outlier_detector/set_of_points.py")
        sc.addFile("s3://yair-marom-tests/outlier_detector/Phones_accelerometer_normalized_0.5_M.csv")
        #sc.addFile("s3://yair-marom-tests/outlier_detector/Phones_gyroscope.csv")
        print("sc finished to load files")
    from coreset_streamer import CoresetStreamer
    from parameters_config import ParameterConfig
    from set_of_points import SetOfPoints
    parameter_config = CoresetSparkExperiment.init_parameter_config()
    if is_amr:
        parameter_config.sample_sizes.append(int(sys.argv[2]))
        parameter_config.sample_sizes.append(int(sys.argv[3]))
    print("machines_number: ", machines_number)
    print("centers_number: ", parameter_config.centers_number)
    print("outliers_number: ", parameter_config.outliers_number)
    total = []
    points_number = parameter_config.points_number
    #data = sc.parallelize([points_number]).map(lambda x: CoresetSparkExperiment.get_data_from_file(x))
    #P = data.collect()[0]
    #P = CoresetSparkExperiment.get_synthetic_data(points_number)
    #P = CoresetSparkExperiment.get_data_from_file(points_number)
    print("started to read lines from file..")
    P = sc.parallelize([1]).map(lambda x: CoresetSparkExperiment.get_data_from_file(points_number)).collect()[0]
    print("finished to read lines from file")
    iterations_number = 1
    coreset_computation_times_total = []
    coreset_computation_times_clean = []
    P_size = P.get_size()
    chunk_size = int(P_size / machines_number)
    for i in range(0, P_size, chunk_size):
        if i + chunk_size >= P_size:
            current_lines = P.get_points_at_indices(i, P_size)
        else:
            current_lines = P.get_points_at_indices(i, i + chunk_size)
        total.append(current_lines)
        P_parallel = sc.parallelize(total)
    CoresetSparkExperiment.run_main_experiment(P, P_parallel)