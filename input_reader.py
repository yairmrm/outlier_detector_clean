from __future__ import division


import csv
import os


class InputReader():


    def __init__(self):
        self.not_empty_rows_list = []

    def get_row_list(self):
        for filename in os.listdir(os.getcwd() + "/dataset"):
            row_index = 1
            with open(os.getcwd() + "/dataset/" + filename, newline='') as csvfile:
                try:
                    readerObj = csv.reader(csvfile, delimiter=',')
                    next(readerObj, None)
                    for row in readerObj:
                        if self.if_row_empty(row):
                            self.not_empty_rows_list.append(row)
                        row_index += 1

                except IOError as ioerr:
                    print(ioerr)

        return self.not_empty_rows_list

    def if_row_empty(self,row):
        for element in row:
            if not element:
                continue
            else:
                return True
        return False
