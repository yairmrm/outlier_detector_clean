from __future__ import division


import csv
import os
from numpy import genfromtxt
from numpy import array as na
#from fancyimpute import KNN



class DataCleaner():

    def __init__(self,row_list):
        self.complete_MTRX = na([])
        self.not_empty_rows_list = row_list

    def clean_data(self):

        # Write clean tmp file
        with open('dataset/tmp_clean_file.csv', 'w', newline='') as tmpfile:
            try:
                wr = csv.writer(tmpfile, quoting=csv.QUOTE_MINIMAL)
                wr.writerows(self.not_empty_rows_list)
            except IOError as ioerr:
                print(ioerr)
        try:
            incomplete_MTRX = genfromtxt('dataset/tmp_clean_file.csv', delimiter=",", skip_header=1, dtype="float")
            #self.complete_MTRX = KNN(k=3).complete(incomplete_MTRX)
            self.complete_MTRX = KNN(k=3).fit_transform(incomplete_MTRX)



        except Exception as e:
            print(e)

        # Remove clean tmp file
        os.remove("dataset/tmp_clean_file.csv")

    def get_complete_matrix(self):
        return self.complete_MTRX
