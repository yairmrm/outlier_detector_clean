import os
import random
import string
import time
import urllib.request
import zipfile
import json
#import matplotlib
import numpy as np
from elasticsearch import Elasticsearch
from pyspark import SparkContext, SparkFiles

#matplotlib.use('TkAgg')
#import matplotlib.pyplot as plt
from outlier_detector import OutliersDetector
from set_of_points import SetOfPoints

sc = es = None


class ODProccessManager:
    def __init__(self, is_reset_indexes = False):
        self.indexes = ['clean','anomalies']
        self.is_reset_indexes = is_reset_indexes
        self.init_context()

    def row_data_to_index_presentation(self,x):
        data = {}
        letters = string.ascii_lowercase
        data['raw_msg'] = x
        data['doc_id'] = ''.join(random.choice(letters) for i in range(20))
        return (data['doc_id'], json.dumps(data))

    def anomaly_to_index_presentation(self,x):
        data = {}
        letters = string.ascii_lowercase
        data['raw_msg'] = str(x)
        data['doc_id'] = ''.join(random.choice(letters) for i in range(20))
        return (data['doc_id'], json.dumps(data))

    def cleane_data_to_index_presentation(self,x):
        data = {}
        letters = string.ascii_lowercase
        data['raw_msg'] = x
        data['doc_id'] = ''.join(random.choice(letters) for i in range(10))
        return (data['doc_id'], json.dumps(data))

    def init_context(self,):
        global sc
        global es
        #if 'elasticsearch-hadoop-7.2.0' not in os.listdir():
        #    es_hadoop = urllib.request.URLopener()
        #    es_hadoop.retrieve("https://artifacts.elastic.co/downloads/elasticsearch-hadoop/elasticsearch-hadoop-7.2.0.zip", "es-hadoop.zip")

        #    with zipfile.ZipFile("es-hadoop.zip", "r") as zip_ref:
        #        zip_ref.extractall()
        os.environ['PYSPARK_SUBMIT_ARGS'] = '--jars /usr/local/spark/spark-2.4.3-bin-hadoop2.7/jars/elasticsearch-hadoop-7.1.1.jar pyspark-shell'
        #os.environ['PYSPARK_SUBMIT_ARGS'] = '--jars /home/outlier_detector_clean/elasticsearch-hadoop-7.2.0/dist/elasticsearch-hadoop-7.2.0.jar pyspark-shell'
        #os.environ['PYSPARK_SUBMIT_ARGS'] = '--jars /spark/jars/elasticsearch-hadoop-6.1.1.jar pyspark-shell'
        es = Elasticsearch('172.18.241.4:9200')
        if self.is_reset_indexes:
            self.reset_all_indexes()
        sc = SparkContext(appName="PythonSparkPCAPWrite")
        sc.setLogLevel("WARN")

    def reset_all_indexes(self, ):
        global sc
        global es
        current_indexes = list(es.indices.get_alias("*").keys())
        for i in range(len(current_indexes)):
            es.indices.delete(current_indexes[i])
        for i in range(len(self.indexes)):
            index_name = self.indexes[i]
            if not es.indices.exists(index_name):
                es.indices.create(index_name)

    def write_data_at_index(self, rdd, index):
        es_write_conf = {
            # specify the node that we are sending data to (this should be the master)
            "es.nodes": '172.18.241.4',

            # specify the port in case it is not the default port
            "es.port": '9200',

            # specify a resource in the form 'index/doc-type'
            "es.resource": index+'/sample',

            # is the input JSON?
            "es.input.json": "yes",

            # is there a field in the mapping that should be used to specify the ES document ID
            "es.mapping.id": "doc_id",
        }

        rdd.saveAsNewAPIHadoopFile(
            path='-',
            outputFormatClass="org.elasticsearch.hadoop.mr.EsOutputFormat",
            keyClass="org.apache.hadoop.io.NullWritable",
            valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
            conf=es_write_conf)

    def read_data_from_index(self, query, index):
        es_read_conf = {
            # specify the node that we are sending data to (this should be the master)
            "es.nodes": "172.18.241.4",

            # specify the read resource in the format 'index/doc-type'
            "es.resource": index+'/sample',

            "es.query": query
        }

        es_rdd = sc.newAPIHadoopRDD(
            inputFormatClass="org.elasticsearch.hadoop.mr.EsInputFormat",
            keyClass="org.apache.hadoop.io.NullWritable",
            valueClass="org.elasticsearch.hadoop.mr.LinkedMapWritable",
            conf=es_read_conf)
        return es_rdd

    def get_row_data(self, data_path):
        rdd = sc.textFile(data_path)
        rdd2 = rdd.map(lambda line: self.row_data_to_index_presentation(line))
        return rdd2

    def cleane_row(self, x,):
        pass

    def clean_row_data_spark(self, rdd):
        clean_rdd = rdd.map(lambda line: self.cleane_row(line))
        #clean_rdd = sc.parallelize(y)
        return clean_rdd

    def detect_anomalies_old(self, rdd):
        data = rdd.collect()
        data_raw = [d[1]['raw_msg'] for d in data]
        points = np.asarray([[float(d) for d in j] for j in data_raw])
        P = SetOfPoints(points)
        error, coreset_total_time, C_outliers, coreset_means = OutliersDetector.detect_via_spark(P, sc)
        print("error: ", error)
        print("coreset_total_time: ", coreset_total_time)
        print("C_outliers: ", C_outliers.points)
        print("coreset_means: ", coreset_means.points)
        anomalies_rdd = sc.parallelize([C_outliers.points])
        anomalies_rdd_2 = sc.parallelize(anomalies_rdd.map(self.anomaly_to_index_presentation).collect())
        return anomalies_rdd_2

    def detect_anomalies_no_spark(self, P):
        P = SetOfPoints(P)
        error, coreset_total_time, C_outliers, coreset_means = OutliersDetector.detect(P)
        if np.isnan(error):
            return np.nan, np.nan, np.nan, np.nan, np.nan
        print("error: ", error)
        print("coreset_total_time: ", coreset_total_time)
        print("C_outliers: ", C_outliers.points)
        print("coreset_means: ", coreset_means.points)

        column_number_to_outliers_indexes = {}
        for column_number in range(coreset_means.dim):
            column_number_to_outliers_indexes[column_number] = []
        means = coreset_means.points
        for outlier_index in C_outliers.indexes:
            p = P.points[outlier_index]
            p_repeat = np.repeat(p.reshape(1, -1), repeats=coreset_means.get_size(), axis=0).reshape(-1, P.dim)
            p_repeat_minus_outliers = p_repeat - means
            squared_norms = np.sum(np.multiply(p_repeat_minus_outliers, p_repeat_minus_outliers), axis=1)
            farthest_center_index = np.argmax(squared_norms)
            outlier_feature_number = np.argmax(np.abs(p_repeat_minus_outliers[farthest_center_index]))
            column_number_to_outliers_indexes[outlier_feature_number].append(outlier_index)

        return error, coreset_total_time, C_outliers, coreset_means, column_number_to_outliers_indexes

    def detect_anomalies_spark(self, rdd):
        error, coreset_total_time, C_outliers, coreset_means = OutliersDetector.detect_via_spark(rdd, sc)
        return error, coreset_total_time, C_outliers, coreset_means

    def save_details(self, P, anomalies, means, selected_features_indexes, folder_path, column_number_to_outliers_indexes, compare_labels):
        timestamp = time.time()
        P = SetOfPoints(P)
        fig_num = 0
        x = list(range(P.get_size()))
        compare_outliers_indexes = np.where(compare_labels == ' true')
        for col_num, outlier_indexes in column_number_to_outliers_indexes.items():
            y = P.points[:, col_num]
            plt.scatter(x, y, marker='o', color='b', s=5)
            plt.scatter(outlier_indexes, P.points[outlier_indexes, col_num], marker='o', color='r', s=20)
            plt.scatter(compare_outliers_indexes, P.points[compare_outliers_indexes, col_num], marker='x', color='g', s=30)
            plt.ylabel('value')
            plt.xlabel('time')
            plt.title("Column number:" + str(selected_features_indexes[col_num]))
            # plt.show()
            plt.savefig(folder_path + "/" + str(fig_num) + "_" + str(timestamp) + ".png")
            plt.close()
            fig_num += 1

        parameters_config = OutliersDetector.parameters_config
        f = open(folder_path + '/' + str(timestamp) + '.txt', "w+")
        f.write("\n\n**** parameters: ****\n\n")
        f.write("parameters_config.sample_size: " + str(parameters_config.sample_size) + "\n")
        f.write("parameters_config.centers_number: " + str(parameters_config.centers_number) + "\n")
        f.write("parameters_config.outliers_number: " + str(parameters_config.outliers_number) + "\n")
        f.write("parameters_config.closest_to_median_rate: " + str(parameters_config.closest_to_median_rate) + "\n")
        f.write("parameters_config.number_of_remains_multiply_factor: " + str(parameters_config.number_of_remains_multiply_factor) + "\n")
        f.write("parameters_config.max_sensitivity_multiply_factor: " + str(parameters_config.max_sensitivity_multiply_factor) + "\n")
        f.write("outliers indexes: " + str(anomalies.indexes) + "\n")
        f.write("outliers normalized: " + str(anomalies.points) + "\n")
        f.write("outliers original: " + str(P.points[anomalies.indexes]) + "\n")
        f.write("centers indexes: " + str(means.indexes) + "\n")
        f.write("centers normalized: " + str(means.points) + "\n")
        f.write("centers original: " + str(P.points[means.indexes]) + "\n")
        f.close()

    def print_and_write_to_file(self, f, s):
        print(s + "\n")
        f.write(s + "\n")

    def scale(self, X, x_min, x_max):
        nom = (X - X.min(axis=0)) * (x_max - x_min)
        denom = X.max(axis=0) - X.min(axis=0)
        denom[denom == 0] = 1
        return x_min + nom / denom

    def scale_spark(self, rdd, x_min, x_max, colums_number):
        mins = []
        maxs = []
        for i in range(colums_number):
            mins.append(rdd.map(lambda row: row[i]).min())
            maxs.append(rdd.map(lambda row: row[i]).max())
        mins = np.asarray(mins)
        maxs = np.asarray(maxs)
        diff = x_max - x_min
        rdd2 = rdd.map(lambda row: (row-mins)*diff)
        denom = maxs - mins
        denom[denom == 0] = 1
        rdd3 = rdd2.map(lambda row: x_min + row / denom)
        return rdd3

def fuc(x,y):
    print("items: ", str(x))
